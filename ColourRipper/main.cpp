#include <stdio.h>
#include <string>

void readFile(void);
void writeFile(void);

FILE* f;



char* matName[64];
float matAmbient[512];
float matDiffuse[512];
float matSpecular[512];
float shiney[64];

int i = 0;
int colIndex = 0;
void main()
{
	f = fopen("colours.txt","r");

	//read the colour components from text file
	readFile();

	//now write it to the cpp file
	writeFile();
}

void readFile()
{
	char input[512];

	while(!feof(f))
	{
		fscanf(f,"%s",input);

		//set the name
		matName[i] = new char[strlen(input)];
		strcpy(matName[i],input);

		//scan the mat properties
		fscanf(f,"%f %f %f %f",&matAmbient[colIndex], &matAmbient[colIndex+1],&matAmbient[colIndex+2],&matAmbient[colIndex+3]);
		fscanf(f,"%f %f %f %f",&matDiffuse[colIndex], &matDiffuse[colIndex+1],&matDiffuse[colIndex+2],&matDiffuse[colIndex+3]);
		fscanf(f,"%f %f %f %f",&matSpecular[colIndex], &matSpecular[colIndex+1],&matSpecular[colIndex+2],&matSpecular[colIndex+3]);

		//increment colIndex
		colIndex+=4;

		//get the shinniness
		fscanf(f,"%f",&shiney[i]);

		//increment i
		i++;

	}

	//close f
	fclose(f);
}

void writeFile(void)
{
//header file----------------------------------------------------------------
	//write the header file
	FILE* hFile = fopen("ncMaterial.h","w");

	//print reference
	fprintf(hFile,"/*AUTO GENERATED VIA COLOURRIPPER (SELF MADE PROGRAM)\nGIT REPO: https://NickC8100@bitbucket.org/NickC8100/colourripper.git \nCOLOUR VALUE REFERENCE: http://www.real3dtutorials.com/tut00008.php */\n");

	//print the start
	fprintf(hFile,"#pragma once\n\nclass ncMaterial\n{\nprivate:\n\tGLfloat amb[4];\n\tGLfloat diff[4];\n\tGLfloat spec[4];\n\tGLfloat shine;\npublic:\n\tncMaterial(void);\n\t~ncMaterial(void);\n");

	//write the one method for setting the material
	fprintf(hFile,"\tvoid setMaterial(void);\n\n");

	//now write the header methods
	for(int j = 0; j < i; j++)
	{
		fprintf(hFile,"\tvoid load%sMaterial(void);\n",matName[j]);
	}

	//close the header file with the closing bracket
	fprintf(hFile,"};\n");

	//close the file
	fclose(hFile);
//end of header file----------------------------------------------------------
//cpp file--------------------------------------------------------------------

	//open the cpp file
	FILE* cppFile = fopen("ncMaterial.cpp","w");

	//print reference
	fprintf(cppFile,"/*AUTO GENERATED VIA COLOURRIPPER (SELF MADE PROGRAM)\nGIT REPO: https://NickC8100@bitbucket.org/NickC8100/colourripper.git \nCOLOUR VALUE REFERENCE: http://www.real3dtutorials.com/tut00008.php */\n");

	//print the start
	fprintf(cppFile,"#include\"stdafx.h\"\n#include\"ncMaterial.h\"\n\nncMaterial::ncMaterial(void)\n{\n\n}\nncMaterial::~ncMaterial(void)\n{\n\n}\n");

	//attributes--
	//amb
	//diff
	//spec
	//shine

	//reset
	colIndex=0;

	//write the method call for setting material
	fprintf(cppFile,"void ncMaterial::setMaterial(void)\n{\n");

	//write loading amb
	fprintf(cppFile,"\tglMaterialfv(GL_FRONT,GL_AMBIENT,amb);\n");
	//write loading amb
	fprintf(cppFile,"\tglMaterialfv(GL_FRONT,GL_DIFFUSE,diff);\n");
	//write loading amb
	fprintf(cppFile,"\tglMaterialfv(GL_FRONT,GL_SPECULAR,spec);\n");
	//write loading amb
	fprintf(cppFile,"\tglMaterialfv(GL_FRONT,GL_SHININESS,&shine);\n");

	//write last brace
	fprintf(cppFile,"}\n");

	//now write the body methods
	for(int j = 0; j < i; j++)
	{
		//write the method call for loading material
		fprintf(cppFile,"void ncMaterial::load%sMaterial(void)\n{\n",matName[j]);

		//write the ambient values
		fprintf(cppFile,"\tamb[0] = %ff; \tamb[1] = %ff; \tamb[2] = %ff; \tamb[3] = %ff;\n",matAmbient[colIndex],matAmbient[colIndex+1],matAmbient[colIndex+2],matAmbient[colIndex+3]);
		//write the diff values
		fprintf(cppFile,"\tdiff[0] = %ff;\tdiff[1] = %ff;\tdiff[2] = %ff;\tdiff[3] = %ff;\n",matDiffuse[colIndex],matDiffuse[colIndex+1],matDiffuse[colIndex+2],matDiffuse[colIndex+3]);
		//write the spec values
		fprintf(cppFile,"\tspec[0] = %ff;\tspec[1] = %ff;\tspec[2] = %ff;\tspec[3] = %ff;\n",matSpecular[colIndex],matSpecular[colIndex+1],matSpecular[colIndex+2],matSpecular[colIndex+3]);
		//write the shiniess
		fprintf(cppFile,"\tshine = %ff;\n",shiney[j]);
		//add the last bracer
		fprintf(cppFile,"}\n");

		//incrememnt colIndex
		colIndex+=4;
	}

	//close the file
	fclose(cppFile);


//end of cpp file------------------------------------------------------------

}