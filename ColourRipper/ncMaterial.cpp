/*AUTO GENERATED VIA COLOURRIPPER (SELF MADE PROGRAM)
GIT REPO: https://NickC8100@bitbucket.org/NickC8100/colourripper.git 
COLOUR VALUE REFERENCE: http://www.real3dtutorials.com/tut00008.php */
#include"stdafx.h"
#include"ncMaterial.h"

ncMaterial::ncMaterial(void)
{

}
ncMaterial::~ncMaterial(void)
{

}
void ncMaterial::setMaterial(void)
{
	glMaterialfv(GL_FRONT,GL_AMBIENT,amb);
	glMaterialfv(GL_FRONT,GL_DIFFUSE,diff);
	glMaterialfv(GL_FRONT,GL_SPECULAR,spec);
	glMaterialfv(GL_FRONT,GL_SHININESS,&shine);
}
void ncMaterial::loadBrassMaterial(void)
{
	amb[0] = 0.329412f; 	amb[1] = 0.223529f; 	amb[2] = 0.027451f; 	amb[3] = 1.000000f;
	diff[0] = 0.780392f;	diff[1] = 0.568627f;	diff[2] = 0.113725f;	diff[3] = 1.000000f;
	spec[0] = 0.992157f;	spec[1] = 0.941176f;	spec[2] = 0.807843f;	spec[3] = 1.000000f;
	shine = 27.897436f;
}
void ncMaterial::loadBronzeMaterial(void)
{
	amb[0] = 0.212500f; 	amb[1] = 0.127500f; 	amb[2] = 0.054000f; 	amb[3] = 1.000000f;
	diff[0] = 0.714000f;	diff[1] = 0.428400f;	diff[2] = 0.181440f;	diff[3] = 1.000000f;
	spec[0] = 0.393548f;	spec[1] = 0.271906f;	spec[2] = 0.166721f;	spec[3] = 1.000000f;
	shine = 25.600000f;
}
void ncMaterial::loadChromeMaterial(void)
{
	amb[0] = 0.250000f; 	amb[1] = 0.250000f; 	amb[2] = 0.250000f; 	amb[3] = 1.000000f;
	diff[0] = 0.400000f;	diff[1] = 0.400000f;	diff[2] = 0.400000f;	diff[3] = 1.000000f;
	spec[0] = 0.774597f;	spec[1] = 0.774597f;	spec[2] = 0.774597f;	spec[3] = 1.000000f;
	shine = 76.800003f;
}
void ncMaterial::loadCopperMaterial(void)
{
	amb[0] = 0.191250f; 	amb[1] = 0.073500f; 	amb[2] = 0.022500f; 	amb[3] = 1.000000f;
	diff[0] = 0.703800f;	diff[1] = 0.270480f;	diff[2] = 0.082800f;	diff[3] = 1.000000f;
	spec[0] = 0.256777f;	spec[1] = 0.137622f;	spec[2] = 0.086014f;	spec[3] = 1.000000f;
	shine = 12.800000f;
}
void ncMaterial::loadEmeraldMaterial(void)
{
	amb[0] = 0.021500f; 	amb[1] = 0.174500f; 	amb[2] = 0.021500f; 	amb[3] = 1.000000f;
	diff[0] = 0.075680f;	diff[1] = 0.614240f;	diff[2] = 0.075680f;	diff[3] = 1.000000f;
	spec[0] = 0.633000f;	spec[1] = 0.727811f;	spec[2] = 0.633000f;	spec[3] = 1.000000f;
	shine = 76.800003f;
}
void ncMaterial::loadGoldMaterial(void)
{
	amb[0] = 0.247250f; 	amb[1] = 0.199500f; 	amb[2] = 0.074500f; 	amb[3] = 1.000000f;
	diff[0] = 0.751640f;	diff[1] = 0.606480f;	diff[2] = 0.226480f;	diff[3] = 1.000000f;
	spec[0] = 0.628281f;	spec[1] = 0.555802f;	spec[2] = 0.366065f;	spec[3] = 1.000000f;
	shine = 51.200001f;
}
void ncMaterial::loadJadeMaterial(void)
{
	amb[0] = 0.135000f; 	amb[1] = 0.222500f; 	amb[2] = 0.157500f; 	amb[3] = 1.000000f;
	diff[0] = 0.540000f;	diff[1] = 0.890000f;	diff[2] = 0.630000f;	diff[3] = 1.000000f;
	spec[0] = 0.316228f;	spec[1] = 0.316228f;	spec[2] = 0.316228f;	spec[3] = 1.000000f;
	shine = 12.800000f;
}
void ncMaterial::loadObsidianMaterial(void)
{
	amb[0] = 0.053750f; 	amb[1] = 0.050000f; 	amb[2] = 0.066250f; 	amb[3] = 1.000000f;
	diff[0] = 0.182750f;	diff[1] = 0.170000f;	diff[2] = 0.225250f;	diff[3] = 1.000000f;
	spec[0] = 0.332741f;	spec[1] = 0.328634f;	spec[2] = 0.346435f;	spec[3] = 1.000000f;
	shine = 38.400002f;
}
void ncMaterial::loadPearlMaterial(void)
{
	amb[0] = 0.250000f; 	amb[1] = 0.207250f; 	amb[2] = 0.207250f; 	amb[3] = 1.000000f;
	diff[0] = 1.000000f;	diff[1] = 0.829000f;	diff[2] = 0.829000f;	diff[3] = 1.000000f;
	spec[0] = 0.296648f;	spec[1] = 0.296648f;	spec[2] = 0.296648f;	spec[3] = 1.000000f;
	shine = 11.264000f;
}
void ncMaterial::loadPlasticBlackMaterial(void)
{
	amb[0] = 0.000000f; 	amb[1] = 0.000000f; 	amb[2] = 0.000000f; 	amb[3] = 1.000000f;
	diff[0] = 0.010000f;	diff[1] = 0.010000f;	diff[2] = 0.010000f;	diff[3] = 1.000000f;
	spec[0] = 0.500000f;	spec[1] = 0.500000f;	spec[2] = 0.500000f;	spec[3] = 1.000000f;
	shine = 32.000000f;
}
void ncMaterial::loadPlasticCyanMaterial(void)
{
	amb[0] = 0.000000f; 	amb[1] = 0.100000f; 	amb[2] = 0.060000f; 	amb[3] = 1.000000f;
	diff[0] = 0.000000f;	diff[1] = 0.509804f;	diff[2] = 0.509804f;	diff[3] = 1.000000f;
	spec[0] = 0.501961f;	spec[1] = 0.501961f;	spec[2] = 0.501961f;	spec[3] = 1.000000f;
	shine = 32.000000f;
}
void ncMaterial::loadPlasticGreenMaterial(void)
{
	amb[0] = 0.000000f; 	amb[1] = 0.000000f; 	amb[2] = 0.000000f; 	amb[3] = 1.000000f;
	diff[0] = 0.100000f;	diff[1] = 0.350000f;	diff[2] = 0.100000f;	diff[3] = 1.000000f;
	spec[0] = 0.450000f;	spec[1] = 0.550000f;	spec[2] = 0.450000f;	spec[3] = 1.000000f;
	shine = 32.000000f;
}
void ncMaterial::loadPlasticRedMaterial(void)
{
	amb[0] = 0.000000f; 	amb[1] = 0.000000f; 	amb[2] = 0.000000f; 	amb[3] = 1.000000f;
	diff[0] = 0.500000f;	diff[1] = 0.000000f;	diff[2] = 0.000000f;	diff[3] = 1.000000f;
	spec[0] = 0.700000f;	spec[1] = 0.600000f;	spec[2] = 0.600000f;	spec[3] = 1.000000f;
	shine = 32.000000f;
}
void ncMaterial::loadPlasticWhiteMaterial(void)
{
	amb[0] = 0.000000f; 	amb[1] = 0.000000f; 	amb[2] = 0.000000f; 	amb[3] = 1.000000f;
	diff[0] = 0.550000f;	diff[1] = 0.550000f;	diff[2] = 0.550000f;	diff[3] = 1.000000f;
	spec[0] = 0.700000f;	spec[1] = 0.700000f;	spec[2] = 0.700000f;	spec[3] = 1.000000f;
	shine = 32.000000f;
}
void ncMaterial::loadPlasticYellowMaterial(void)
{
	amb[0] = 0.000000f; 	amb[1] = 0.000000f; 	amb[2] = 0.000000f; 	amb[3] = 1.000000f;
	diff[0] = 0.500000f;	diff[1] = 0.500000f;	diff[2] = 0.000000f;	diff[3] = 1.000000f;
	spec[0] = 0.600000f;	spec[1] = 0.600000f;	spec[2] = 0.500000f;	spec[3] = 1.000000f;
	shine = 32.000000f;
}
void ncMaterial::loadRubberBlackMaterial(void)
{
	amb[0] = 0.020000f; 	amb[1] = 0.020000f; 	amb[2] = 0.020000f; 	amb[3] = 1.000000f;
	diff[0] = 0.010000f;	diff[1] = 0.010000f;	diff[2] = 0.010000f;	diff[3] = 1.000000f;
	spec[0] = 0.400000f;	spec[1] = 0.400000f;	spec[2] = 0.400000f;	spec[3] = 1.000000f;
	shine = 10.000000f;
}
void ncMaterial::loadRubberCyanMaterial(void)
{
	amb[0] = 0.000000f; 	amb[1] = 0.050000f; 	amb[2] = 0.050000f; 	amb[3] = 1.000000f;
	diff[0] = 0.400000f;	diff[1] = 0.500000f;	diff[2] = 0.500000f;	diff[3] = 1.000000f;
	spec[0] = 0.040000f;	spec[1] = 0.700000f;	spec[2] = 0.700000f;	spec[3] = 1.000000f;
	shine = 10.000000f;
}
void ncMaterial::loadRubberGreenMaterial(void)
{
	amb[0] = 0.000000f; 	amb[1] = 0.050000f; 	amb[2] = 0.000000f; 	amb[3] = 1.000000f;
	diff[0] = 0.400000f;	diff[1] = 0.500000f;	diff[2] = 0.400000f;	diff[3] = 1.000000f;
	spec[0] = 0.040000f;	spec[1] = 0.700000f;	spec[2] = 0.040000f;	spec[3] = 1.000000f;
	shine = 10.000000f;
}
void ncMaterial::loadRubberRedMaterial(void)
{
	amb[0] = 0.050000f; 	amb[1] = 0.000000f; 	amb[2] = 0.000000f; 	amb[3] = 1.000000f;
	diff[0] = 0.500000f;	diff[1] = 0.400000f;	diff[2] = 0.400000f;	diff[3] = 1.000000f;
	spec[0] = 0.700000f;	spec[1] = 0.040000f;	spec[2] = 0.040000f;	spec[3] = 1.000000f;
	shine = 10.000000f;
}
void ncMaterial::loadRubberWhiteMaterial(void)
{
	amb[0] = 0.050000f; 	amb[1] = 0.050000f; 	amb[2] = 0.050000f; 	amb[3] = 1.000000f;
	diff[0] = 0.500000f;	diff[1] = 0.500000f;	diff[2] = 0.500000f;	diff[3] = 1.000000f;
	spec[0] = 0.700000f;	spec[1] = 0.700000f;	spec[2] = 0.700000f;	spec[3] = 1.000000f;
	shine = 10.000000f;
}
void ncMaterial::loadRubberYellowMaterial(void)
{
	amb[0] = 0.050000f; 	amb[1] = 0.050000f; 	amb[2] = 0.000000f; 	amb[3] = 1.000000f;
	diff[0] = 0.500000f;	diff[1] = 0.500000f;	diff[2] = 0.400000f;	diff[3] = 1.000000f;
	spec[0] = 0.700000f;	spec[1] = 0.700000f;	spec[2] = 0.040000f;	spec[3] = 1.000000f;
	shine = 10.000000f;
}
void ncMaterial::loadRubyMaterial(void)
{
	amb[0] = 0.174500f; 	amb[1] = 0.011750f; 	amb[2] = 0.011750f; 	amb[3] = 1.000000f;
	diff[0] = 0.614240f;	diff[1] = 0.041360f;	diff[2] = 0.041360f;	diff[3] = 1.000000f;
	spec[0] = 0.727811f;	spec[1] = 0.626959f;	spec[2] = 0.626959f;	spec[3] = 1.000000f;
	shine = 76.800003f;
}
void ncMaterial::loadSilverMaterial(void)
{
	amb[0] = 0.192250f; 	amb[1] = 0.192250f; 	amb[2] = 0.192250f; 	amb[3] = 1.000000f;
	diff[0] = 0.507540f;	diff[1] = 0.507540f;	diff[2] = 0.507540f;	diff[3] = 1.000000f;
	spec[0] = 0.508273f;	spec[1] = 0.508273f;	spec[2] = 0.508273f;	spec[3] = 1.000000f;
	shine = 51.200001f;
}
void ncMaterial::loadTurquoiseMaterial(void)
{
	amb[0] = 0.100000f; 	amb[1] = 0.187250f; 	amb[2] = 0.174500f; 	amb[3] = 1.000000f;
	diff[0] = 0.396000f;	diff[1] = 0.741510f;	diff[2] = 0.691020f;	diff[3] = 1.000000f;
	spec[0] = 0.297254f;	spec[1] = 0.308290f;	spec[2] = 0.306678f;	spec[3] = 1.000000f;
	shine = 12.800000f;
}
