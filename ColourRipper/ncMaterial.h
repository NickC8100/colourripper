/*AUTO GENERATED VIA COLOURRIPPER (SELF MADE PROGRAM)
GIT REPO: https://NickC8100@bitbucket.org/NickC8100/colourripper.git 
COLOUR VALUE REFERENCE: http://www.real3dtutorials.com/tut00008.php */
#pragma once

class ncMaterial
{
private:
	GLfloat amb[4];
	GLfloat diff[4];
	GLfloat spec[4];
	GLfloat shine;
public:
	ncMaterial(void);
	~ncMaterial(void);
	void setMaterial(void);

	void loadBrassMaterial(void);
	void loadBronzeMaterial(void);
	void loadChromeMaterial(void);
	void loadCopperMaterial(void);
	void loadEmeraldMaterial(void);
	void loadGoldMaterial(void);
	void loadJadeMaterial(void);
	void loadObsidianMaterial(void);
	void loadPearlMaterial(void);
	void loadPlasticBlackMaterial(void);
	void loadPlasticCyanMaterial(void);
	void loadPlasticGreenMaterial(void);
	void loadPlasticRedMaterial(void);
	void loadPlasticWhiteMaterial(void);
	void loadPlasticYellowMaterial(void);
	void loadRubberBlackMaterial(void);
	void loadRubberCyanMaterial(void);
	void loadRubberGreenMaterial(void);
	void loadRubberRedMaterial(void);
	void loadRubberWhiteMaterial(void);
	void loadRubberYellowMaterial(void);
	void loadRubyMaterial(void);
	void loadSilverMaterial(void);
	void loadTurquoiseMaterial(void);
};
